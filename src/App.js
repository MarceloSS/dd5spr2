import './App.css';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

function App() {

  const scheme = yup.object().shape({
    name : yup.string().min(4, "Minimo 4 digitos").required("Campo obrigatorio"),
    age : yup.number().moreThan(0, "numero invalido").required("Campo obrigatorio"),
    email : yup.string().email("Email invalido").required("Campo obrigatorio"),
    password : yup.string().min(8, "Minimo de 8 digitos").required("Campo obrigatorio"),
    passwordConfirm : yup.string().oneOf([yup.ref("password")], "Senhas diferentes").required(),
  });

  const {register, handleSubmit, errors} = useForm(
    { resolver : yupResolver(scheme) }
  );

  const handleForm = (user) => {
    console.log(user)
  };

  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={ handleSubmit(handleForm) }>
          <div>
            <input placeholder="Nome" name ="name" ref={ register } ></input>
            <p style={ {color:"red"} }> { errors.name?.message } </p>
          </div>
          <div>
            <input placeholder="Idade" name ="age" ref={ register } type="Number"></input>
            <p style={ {color:"red"} }> { errors.age?.message } </p>
          </div>
          <div>
            <input placeholder="Email" name ="email" ref={ register } ></input>
            <p style={ {color:"red"} }> { errors.email?.message } </p>
          </div>
          <div>
            <input placeholder="Senha" name="password" ref={ register }></input>
            <p style={ {color:"red"} }> { errors.password?.message } </p>
          </div>
          <div>
            <input placeholder="Confirmar Senha" name ="passwordConfirm" ref={ register } ></input>
            <p style={ {color:"red"} }> { errors.passwordConfirm?.message } </p>
          </div>
          <div>
            <button type="submit">Enviar</button>
          </div>
        </form>
      </header>
    </div>
  );
}

export default App;
